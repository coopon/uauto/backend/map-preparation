The process of preparing map for uauto is described [here](https://coopon.purambokku.me/doku.php?id=prepare-map-android). This shell script automates this process.

All you need to do is change the value of few variables defined in UPPERcase in the shell script according to your region of interest.

**External Dependency**

1. zip (apt/dnf install zip should work).

By default the script targets Pondicherry region. Just invoke like following and everything will be taken care of.

    ./prepare.sh
