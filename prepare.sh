#!/bin/bash

set -e

# minlon,minlat,maxlon,maxlat
BBOX="79.7460000,11.8311000,79.8775000,12.0524000"
PLACE="pondicherry"

#----------------------

# osm raw data variables
DATA_DIR=`pwd`                                  # where to store?
DATA_LOC="build/"$(date '+%d-%b-%Y')            # where to store?
DATA_FILE_NAME=$PLACE".osm"                     # under what name to store?

# osmosis data variables
MAP_FILE_NAME=$PLACE".map"


# -------------------------------------------	

# defining functions
preliminary_checks()
{
  if [ ! -d $1/$2 ]; then
    echo "creating build directory."
    mkdir -p $1/$2
  fi
}


download_osm_data()
{
  SOURCE="https://overpass-api.de/api/map?bbox="
  
  if [ -f $2/$3/$4 ]; then
    read -p "$4 already exists. Do you want to overwrite? (y/n) " CHOICE
    
    if [ $CHOICE = "n" ]; then
        echo "continuing with existing file. Not Downloading."
        return 0;
    else
        echo "Overwriting existing file.";
    fi
  fi
  
  echo -e "\n\n---------------------------"
  echo "Downloading data from OpenStreetMap ..."
  echo "---------------------------"
  
  wget -c $SOURCE$1 --output-document=$2/$3/$4
 
}

download_osmosis()
{
  if [ ! -d "osmosis" ]; then
    echo -e "\n\n---------------------------"
    echo "Downloading OSMOSIS ..."
    echo "---------------------------"
    
    wget -c https://bretth.dev.openstreetmap.org/osmosis-build/osmosis-latest.tgz
    mkdir osmosis
  
    echo "Extracting osmosis ..."
    tar xf osmosis-latest.tgz -C osmosis && rm osmosis-latest.tgz
    chmod a+x osmosis/bin/osmosis
    
    # download mapsforge-writer-plugin
    echo -e "\n\n---------------------------"
    echo "Downloading mapsforge-writer-plugin ..."
    echo "---------------------------"
    
    wget -c "http://downloads.purambokku.me/mapsforge/mapsforge-map-writer-master-SNAPSHOT-jar-with-dependencies.jar"
    mv mapsforge-map-writer-master-SNAPSHOT-jar-with-dependencies.jar osmosis/lib/default/
  fi
}

download_graphhopper()
{
    if [ ! -d "graphhopper" ]; then
      echo -e "\n\n---------------------------"
      echo "Downloading Graphhopper ..."
      echo "---------------------------"
      
      wget -c "https://github.com/graphhopper/graphhopper/archive/0.10.0.tar.gz" -O graphhopper.tar.gz
      mkdir graphhopper
      
      echo -e "\n\n---------------------------"
      echo "Extracting Graphhopper ..."
      echo "---------------------------"
      
      tar -xf graphhopper.tar.gz --strip-components=1 -C graphhopper && rm graphhopper.tar.gz
    fi
}

generate_binary_map()
{
    if [ ! -f $1/$2/$4 ]; then
      echo -e "\n\n---------------------------"   
      echo "Generating binary map"
      echo "---------------------------"
      
      ./osmosis/bin/osmosis --rx file=$1/$2/$3 --mw file=$1/$2/$4
    else
      echo "$4 already exists. Not Overwriting."
    fi
}

generate_routing_data()
{
  if [ ! -d $1/$2/$4"-gh" ]; then
    echo -e "\n\n---------------------------"
    echo "Generating routing data ..."
    echo "---------------------------"
    
    cd graphhopper
    ./graphhopper.sh import $1/$2/$3
  fi
}

compress_to_file()
{
  echo -e "\n\n---------------------------"
  echo "compressing to file ..."
  echo "---------------------------"
  
  cd $1/$2
  cp $3 $4"-gh"
  zip -r $4"-gh.zip" $4"-gh"
}

# -------------------------------------------

# calling function for execution
preliminary_checks $DATA_DIR $DATA_LOC
download_osm_data $BBOX $DATA_DIR $DATA_LOC $DATA_FILE_NAME
download_osmosis
download_graphhopper

# conversion function calls
generate_binary_map $DATA_DIR $DATA_LOC $DATA_FILE_NAME $MAP_FILE_NAME
generate_routing_data $DATA_DIR $DATA_LOC $DATA_FILE_NAME $PLACE

# TODO generate waysdb with python script
# compress into zip file
compress_to_file $DATA_DIR $DATA_LOC $MAP_FILE_NAME $PLACE 

echo -e "\n\n****DONE****"
